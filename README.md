# otel-gen

Sample application instrumented with OTEL metrics, used to generate example telemetry. Has support for the following metric-types:
* Gauge
* Sum/Counter
* Histogram
* ExponentialHistogram

## Running the binary
```
go run main.go
```

### Recording metrics instrumented on the server
```
curl localhost:8081/generate
```


