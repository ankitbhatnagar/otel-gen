package main

import (
	"context"
	"log"
	"math/rand"
	"net/http"
	"time"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdoutmetric"
	api "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
)

func customAggregationSelector(kind metric.InstrumentKind) metric.Aggregation {
	if kind == metric.InstrumentKindHistogram {
		return metric.AggregationBase2ExponentialHistogram{
			MaxSize:  5,
			MaxScale: 2,
			NoMinMax: false,
		}
	}
	return metric.DefaultAggregationSelector(kind)
}

func main() {
	stdout := false // only print to stdout
	rng := rand.New(rand.NewSource(time.Now().UnixNano()))
	opt := api.WithAttributes(
		attribute.Key("A").String("B"),
		attribute.Key("C").String("D"),
	)

	resources := resource.NewWithAttributes(
		semconv.SchemaURL,
		semconv.ServiceNameKey.String("service"),
		semconv.ServiceVersionKey.String("v0.0.0"),
	)

	headers := map[string]string{"x-target-projectid": "12345"}

	// setup OTEL exporter
	ctx := context.Background()
	var (
		metricExporter metric.Exporter
		err            error
	)
	if stdout {
		metricExporter, err = stdoutmetric.New(
			stdoutmetric.WithPrettyPrint(),
			stdoutmetric.WithAggregationSelector(customAggregationSelector),
		)
	} else {
		metricExporter, err = otlpmetrichttp.New(
			ctx,
			otlpmetrichttp.WithInsecure(),
			otlpmetrichttp.WithEndpoint("localhost:4318"),
			otlpmetrichttp.WithCompression(otlpmetrichttp.GzipCompression),
			otlpmetrichttp.WithHeaders(headers),
			otlpmetrichttp.WithAggregationSelector(customAggregationSelector),
		)
	}
	if err != nil {
		log.Fatalln(err)
	}

	meterProvider := metric.NewMeterProvider(
		metric.WithResource(resources),
		metric.WithReader(metric.NewPeriodicReader(
			metricExporter,
			metric.WithInterval(10*time.Second),
		)),
	)
	defer func() {
		err := meterProvider.Shutdown(context.Background())
		if err != nil {
			log.Fatalln(err)
		}
	}()

	// Create an instance on a meter for the given instrumentation scope
	meter := meterProvider.Meter(
		"observe.gitlab.com",
		api.WithInstrumentationVersion("v0.0.0"),
	)
	gauge, err := meter.Float64ObservableGauge(
		"bar",
		api.WithDescription("Random gauge"))
	if err != nil {
		log.Fatal(err)
	}
	_, err = meter.RegisterCallback(func(_ context.Context, o api.Observer) error {
		n := -10. + rng.Float64()*(90.) // [-10, 100)
		o.ObserveFloat64(gauge, n, opt)
		return nil
	}, gauge)
	if err != nil {
		log.Fatal(err)
	}

	// Create two synchronous instruments: counter and histogram
	requestCount, err := meter.Int64Counter(
		"request_count",
		api.WithDescription("Incoming request count"),
	)
	if err != nil {
		log.Fatalln(err)
	}
	requestDuration, err := meter.Float64Histogram(
		"duration",
		api.WithDescription("Incoming end to end duration"),
	)
	if err != nil {
		log.Fatalln(err)
	}

	http.HandleFunc("/generate", func(w http.ResponseWriter, req *http.Request) {
		requestStartTime := time.Now()
		_, _ = w.Write([]byte("Generated OK\n"))
		elapsedTime := float64(time.Since(requestStartTime)) / float64(time.Millisecond)
		// record measurements
		requestCount.Add(ctx, 10, opt)
		requestDuration.Record(ctx, elapsedTime, opt)
	})

	http.ListenAndServe(":8081", nil)
}
